pragma solidity ^0.4.23;

import "./DateTime.sol";


contract Freelancer {
    struct ApplicantInfo {
        uint payment;
        string message;
        uint updateCounter;
        bool dropped;
    }

    uint constant DEPOSIT_RATIO = 20;

    address public hirer;
    address public applicant;
    uint public hirer_deposit;
    uint public applicant_deposit;

    uint public balance;

    mapping (address => ApplicantInfo) public ApplicantList;
    mapping (uint => address) ApplicantIndex;
    uint public applicantCounter = 0;

    string public title;
    string public description;
    uint public deadline;
    uint public depositEndTime;
    uint public intendedPayment;
    uint public depositValue;
    // uint public creationDate;

    string[] private resources; // links of resources
    uint private resourceKey; // key to unlock resource
    uint public resourceNumber = 0;

    string[] private products; // links of products
    uint private productKey; // key to unlock product
    uint public productNumber = 0;
    bool public productDone = false;

    DateTime public datetime;


    /*
    ============================ EVENTS ============================
    */

    event PlaceDeposit(address who, uint money);
    event DepositNotMeet();
    event Start();
    event UpdateResource();
    event UpdateProduct();


    /*
    =========================== MODIFIER ===========================
    */

    modifier condition(bool _condition) {
        require(_condition);
        _;
    }

    modifier onlyHirer() {
        require(
            msg.sender == hirer,
            "Only hirer can call this."
        );
        _;
    }

    modifier onlyApplicant() {
        require(
            msg.sender == applicant,
            "Only applicant can call this."
        );
        _;
    }


    /*
    =========================== CONTRACT ===========================
    */

    constructor(address datetimeAddress) public {
        hirer = msg.sender;
        datetime = DateTime(datetimeAddress);
        // creationDate = now;
    }

    function setTitle(string _title) public 
        onlyHirer
    {
        title = _title;
    }

    function setDescription(string _desc) public
        onlyHirer
    {
        description = _desc;
    }

    function setIntendedPayment(uint _payment) public
        onlyHirer
    {
        intendedPayment = _payment;
        depositValue = DEPOSIT_RATIO * intendedPayment / 100;
    }

    function setDeadline(uint16 year, uint8 month, uint8 day, uint8 hour) public
        onlyHirer
    {
        deadline = datetime.toTimestamp(year, month, day, hour);
    }

    function setDepositEndTime(uint16 year, uint8 month, uint8 day, uint8 hour) public
        onlyHirer
    {
        depositEndTime = datetime.toTimestamp(year, month, day, hour);
    }

    function placeDeposit(uint time) public payable
        condition(intendedPayment != 0)
        condition(msg.value == depositValue)
        condition(time < depositEndTime)
    {
        if (msg.sender == hirer) {
            // one time deposit
            require(hirer_deposit == 0);
            hirer_deposit = msg.value;

            emit PlaceDeposit(msg.sender, msg.value);
        }

        else if (msg.sender == applicant) {
            // one time deposit
            require(applicant_deposit == 0);
            applicant_deposit = msg.value;

            emit PlaceDeposit(msg.sender, msg.value);
        }

        else {
            // who are you?
        }
    }

    function pay() public payable
        onlyHirer
        condition(msg.value > 0)
        condition(intendedPayment > balance + depositValue)
    {
        balance += msg.value;
    }

    function depositOk() private view returns(bool) {
        return hirer_deposit >= depositValue && applicant_deposit >= depositValue;
    }

    function payOk() private view returns(bool) {
        return depositOk() && balance + depositValue >= intendedPayment;
    }

    function getResource(uint index, uint _key) public view
        condition(resourceKey == _key)
        condition(index < resources.length)
    returns(string)
    {
        return resources[index];
    }

    function getProduct(uint index, uint _key) public view
        condition(productKey == _key)
        condition(index < products.length)
    returns(string)
    {
        return products[index];
    }

    function checksumKey(string[] data) private pure returns(uint) {
        uint _key = 0;
        for (uint i = 0; i < data.length; i++)
            _key += uint(keccak256(data[i]));
        return _key;
    }

    // hirer or applicant may call this as anytime they want
    function deadline(uint time) public {
        if (time >= deadline + 1 weeks && (payOk() == false || !productDone)) {
            // over deadline and boom
            returnMoney();
            return;
        }

        if (payOk() && productDone) {
            return;
        }
    }

    /*
    =========================== APPLICANT ===========================
    */

    function apply(uint payment, string message) public {
        require(msg.sender != hirer);
        if (ApplicantList[msg.sender].updateCounter == 0) {
            ApplicantIndex[applicantCounter] = msg.sender;
            applicantCounter++;
        }

        ApplicantList[msg.sender].payment = payment;
        ApplicantList[msg.sender].message = message;
        ApplicantList[msg.sender].updateCounter++;
    }

    function drop() public {
        ApplicantList[msg.sender].dropped = true;
    }

    function getApplicantInfo(uint index) public view returns(uint, string) {
        ApplicantInfo storage info = ApplicantList[ApplicantIndex[index]];
        if (info.dropped == true)
            return (0, "");
        return (info.payment, info.message);
    }

    function addProduct(string _product) public 
        onlyApplicant
    returns(uint)
    {
        products.push(_product);
        emit UpdateProduct();

        productNumber++;
        productKey = checksumKey(products);
        return productKey;
    }

    function updateProduct(string _product, uint index) public
        onlyApplicant
    returns(uint)
    {
        require(index < products.length);
        emit UpdateProduct();

        products[index] = _product;
        productKey = checksumKey(products);
        return productKey;
    }

    function getResourceKey() public view 
        onlyApplicant
        condition(depositOk())
    returns(uint)
    {
        return resourceKey;
    }

    function setProductStatus() public
        onlyApplicant
        condition(productNumber > 0)
    {
        productDone = true;
    }


    /*
    =========================== HIRER ===========================
    */

    function chooseApplicant(uint index) public
        onlyHirer
    {
        ApplicantInfo storage info = ApplicantList[ApplicantIndex[index]];
        if (info.dropped == true)
            return;
        applicant = ApplicantIndex[index];
        setIntendedPayment(ApplicantList[applicant].payment);
    }

    function addResource(string _resource) public 
        onlyHirer
    returns(uint)
    {
        resources.push(_resource);
        emit UpdateResource();

        resourceNumber++;
        resourceKey = checksumKey(resources);
        return resourceKey;
    }

    function updateResource(string _resource, uint index) public
        onlyHirer
    returns(uint)
    {
        require(index < resources.length);
        emit UpdateResource();

        resources[index] = _resource;
        resourceKey = checksumKey(resources);
        return resourceKey;
    }

    function start(uint time) public
        onlyHirer
    {
        bool depositResult = depositOk();
        if (time >= depositEndTime && depositResult == false) {
            emit DepositNotMeet();
            returnMoney();
            // selfdestruct(this);
            return;
        }
        require(depositResult, "Deposit is not ok");
        emit Start();
    }

    function getProductKey() public view
        onlyHirer
        condition(payOk())
    returns(uint)
    {
        return productKey;
    }

    /*
    =========================== GET MONEY ===========================
    */

    function getBalance() public view returns(uint) {
        // return this.balance;
    }

    function returnMoney() private {
        hirer.transfer(hirer_deposit);
        applicant.transfer(applicant_deposit);
        hirer.transfer(balance);
    }

    function withdraw() public
        onlyApplicant
        condition(payOk())
        condition(productDone == true)
    {
        applicant.transfer(hirer_deposit);
        applicant.transfer(applicant_deposit);
        applicant.transfer(balance);
    }
}