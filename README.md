# READ ME

---
Consult this: [version1](https://ll4smartcontracts.slack.com/files/UATJL02KY/FB13GR1C7/linearlogic.pdf)

or this: [version2?](https://nganhkhoa.github.io/2018/06/06/freelancer-on-blockchain/)

---

## How to test

### Naive test on Remix Editor

#### Setup

Go to [remix editor](http://remix.ethereum.org/).

Add two new files, name them, `Freelancer.sol` and `DateTime.sol`. Add the content using the files on `./Contract/`.

On the right tab, choose the compile option. Set auto compile. Choose run tab. On the Environment, use `Javascript VM`.

Look and notice, in the Account, you can see a list of accounts.

Below there is a contract select dropdown.

Deploy a DateTime contract first, below, it creates a contract_address. Copy its address.

Choose the contract Freelancer and in deploy with DateTime contract address coppied as argument.

Now a Freelancer contract is created. Where the account being chosen is the owner.

#### Setup a job request and apply

You can now set title, description, .... The interface is:

```solidity
function setTitle(string _title) public;
function setDescription(string _desc) public;
function setIntendedPayment(uint _payment) public;
function setDeadline(uint16 year, uint8 month, uint8 day, uint8 hour) public;
function setDepositEndTime(uint16 year, uint8 month, uint8 day, uint8 hour) public;
```

Most info are public so after set, you can see your variables.

```solidity
string public title;
string public description;
uint public deadline;
uint public depositEndTime;
uint public intendedPayment;
```

Time variable are `unix timestamp`. Conversion can be done here: [epochconverter.com](https://www.epochconverter.com/).

Now you need to add a applicant to this contract.

Choose another account and call apply:

```solidity
function apply(uint payment, string message) public;
```

You can have as many applicants as you want. The hirer will choose only one to work with.

All the information is publicly accessible.

```solidity
mapping (address => ApplicantInfo) public ApplicantList;
mapping (uint => address) ApplicantIndex;
uint public applicantCounter = 0;
```

Call ApplicantIndex to get the address and get the ApplicantInfo using ApplicantList.

Now, as the hirer, choose an applicant using index of him.

```solidity
function chooseApplicant(uint index) public;
```

#### Placing deposit

When the hirer choose an applicant, the intendedPayment is modified to be the amount the applicant suggest.

Both must now place their deposit part in the contract.

To add money, make sure the value field is not 0 and call `deposit()` with time (as unix timestamp) as argument. The time argument is the time when the user send the deposit call. In applicantion level, time is set using our backend and call the contract.

```solidity
function placeDeposit(uint time) public payable;
```

You can check the money of the hirer and applicant using:

```solidity
uint public hirer_deposit;
uint public applicant_deposit;
```

Before sending deposit, makesure that you are using the corresponding account and deposit value must be 20% to the `intendedPayment`. This is a one time call.

After both sides have sent their deposit part, the hirer call `start` to check for precondition. In the applicant level, when a user deposit money, our backend will receive an `event` and immediately call `start`.

```solidity
function start(uint time) public;
```

Again, time is the time when call this function.

#### Adding resources and product, and get key

The hire must add resources, as link, to the server. He can update the resources, and can also view it using variables.

```solidity
string[] private resources; // links of resources
uint public resourceNumber = 0;

function addResource(string _resource) public;
function updateResource(string _resource, uint index) public;
```

In the same maner, the applicant can update his product.

```solidity
string[] private products; // links of products
uint public productNumber = 0;
bool public productDone = false;


function addProduct(string _product) public;
function updateProduct(string _product, uint index) public;

// forget to check name
// this set productDone = true
function setProductStatus() public;
```

Only the hirer can get the `productKey`, and only the applicant can get the `resourceKey`. And using the key, only they can request for resource/product by using the index in the string[].

```solidity
uint private resourceKey; // key to unlock resource
uint private productKey; // key to unlock product

function getResourceKey() public view;
function getProductKey() public view;


function getResource(uint index, uint _key) public view;
function getProduct(uint index, uint _key) public view;
```

#### Payment and Deadline

The hirer must call pay to pay for the product after deadline + 1 week. He can pay many time, as long as the sum is equal to the remains (deposit is accumulated).

```solidity
function pay() public payable;
```

The pay function doesn't need a timestamp because we don't care.

When the deadline reaches, either hirer or applicant may call the deadline function to check for deadline. If deadline has passed for 1 week and one of the sides has not finished their work/payment, the money is returned. Otherwise, the applicant will receive his reward and the hirer can get the product.

> Maybe in the next update, the applicant has two types of product and only if he has paid, he can get the real product.

```solidity
function deadline(uint time) public {
    if (time >= deadline + 1 weeks && (payOk() == false || !productDone)) {
        // over deadline and boom
        returnMoney();
        return;
    }

    if (payOk() && productDone) {
        return;
    }
}
```

### Test using truffle

Later mate

## Test cases

- [ ] Create job
- [ ] Apply job
- [ ] Accept applicant
- [ ] Choose a dropped applicant
- [ ] Deposit ontime
- [ ] Deposit over time
- [ ] Start when deposit is not ok
- [ ] Add resource and product
- [ ] Pay
- [ ] Call deadline when not deadline (with work done/undone)
- [ ] Call deadline when deadline (with work done/undone)
- [ ] ...